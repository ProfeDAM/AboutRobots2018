package com.exemple.profedam.aboutrobots2017;

import android.app.Activity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class RobotActivity extends Activity {

    private int clickCounter = 0;
    Button btnNoTocar;
    TextView textView;
    ImageView imageView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_robot);
        btnNoTocar = findViewById(R.id.btnNoTocar);
        textView = findViewById (R.id.textView);
        imageView = findViewById (R.id.imageView);


    }

    /* Método que se ejecuta cuando algún botón vigilado
     por esta clase es pulsado */

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        this.clickCounter = savedInstanceState.getInt("clickCounter");
        refreshScreen();

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("clickCounter", this.clickCounter);


    }

    public void onClick(View v) {

        clickCounter++;
        refreshScreen();







    }

    private void refreshScreen() {



        if (clickCounter==1)
 {
     btnNoTocar.setText (getString(R.string.message_final_button));
     textView.setText (getString(R.string.message_final_text));
     imageView.setImageResource(R.drawable.angry_robot_canvas);

     /* TODO Cal treure els strings al fitxer de strings */
 }

        if (clickCounter==2)
        {
            btnNoTocar.setVisibility(View.INVISIBLE);
            textView.setText (getString(R.string.message_final_button));
            imageView.setImageResource(R.drawable.angry_robot_canvas);
        }
    }
}
